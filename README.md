Deploying Continuously with CICD Pipelines and Kubernetes
=========================================================

Documentation and exercises for the conference workshop:

https://devopscon.io/continuous-delivery-automation/ci-cd-workshop-deploying-continuously-with-ci-cd-pipelines-kubernetes-2/

The compiled documentation can be found here:

https://k8s-workshop-devopscon.gitlab.io/deploying-continuously-with-cicd-pipelines-and-kubernetes/
