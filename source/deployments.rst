Deployment Startegies
=====================

We begin by deploying the simple application we created with a ``Deployment`` manifest
and a local ``Service``. We build the application with::

   $ make docker-build docker-push VERSION=0.1

Now, we set up a deployment and service:

.. code:: yaml

   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: green-nginx-app
     labels:
       app: green-nginx
   spec:
     replicas: 10
     selector:
       matchLabels:
         app: green-nginx
     template:
       metadata:
         labels:
           app: green-nginx
           version: v0.1
       spec:
         containers:
         - name: nginx
           image: docker.io/oz123/app:v0.1
           ports:
           - name: http
             containerPort: 80
           env:
           - name: APP_VERSION
             value: v0.1
           livenessProbe:
             httpGet:
               path: /version
               port: http
             initialDelaySeconds: 5
             periodSeconds: 5

and the ``Service``:

.. code:: yaml

  apiVersion: v1
  kind: Service
  metadata:
    name: green-nginx
    labels:
      app: green-nginx
  spec:
    ports:
    - name: http
      port: 80
      targetPort: http
    selector:
      app: green-nginx


You will will notice that only 4 pods out of 10 will start. What is going on here?

.. code::

   # k get deployments.apps  
   NAME              READY   UP-TO-DATE   AVAILABLE   AGE
   green-nginx-app   4/10    4            4           4m28s
   
The solution can be found with ``k describe pod -l app=green-nginx``.

Hint: use `k get resourcequotas` and also `k top pods`.

After you verfied all the pods are up to date check that the service is working with::

    $ curl green-nginx.<your-namespace>.svc.cluster.local

Also, checkout the ``strategy`` section in the deployment. Although this was not specified,
it was explicitly added, since it's the default::

    strategy:
      rollingUpdate:
        maxSurge: 25%
        maxUnavailable: 25%
      type: RollingUpdate

Let's see what happens when we deploy a version with explicit rolling strategy:

.. code:: yaml

   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: green-nginx-app
     labels:
       app: green-nginx
   spec:
     replicas: 10
     selector:
       matchLabels:
         app: green-nginx
     strategy:
       rollingUpdate:
         maxSurge: 10%
         maxUnavailable: 10%
       type: RollingUpdate
     template:
       metadata:
         labels:
           app: green-nginx
           version: v0.1
       spec:
         containers:
         - name: nginx
           resources:
             limits:
               cpu: "0.1"
               memory: "100Mi"
             requests:
               cpu: "0.1"
               memory: "100Mi"
           image: docker.io/oz123/app:v0.1
           ports:
           - name: http
             containerPort: 80
           livenessProbe:
             httpGet:
               path: /version
               port: http
             initialDelaySeconds: 5
             periodSeconds: 5

And then update just the image to be version 0.2 (this version is hardcoded, and does not read
the environment variable as in v0.1). Use ``k get pods -w`` to see how the new version is released.

Now, let's check what happens when we change the strategy to ``Recreate``::

   $ kubectl set image deployment green-nginx-app nginx=docker.io/oz123/app:v0.2 && k get pod -w

What can this startegy be useful?

See: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
