Parts
=====
Part 1: Presentation and discussion: what problem do continuous integration and
continuous deployment solve?

Deploying software directly to production sounds like a crazy idea.
However, more often than not, this should be the default. Even in organizations
where a CICD pipeline is established, there are still multiple "environments":
the production environment, where the actual business is happening, the staging
and maybe even development environments, where supposedly some QA is
taking place, and, of course, there is always "it works on my laptop".
In such organizations despite, having a CI/CD pipeline, software is eventually
deployed by hand.

Part 2: Should we prefer deploying straight to production and how?
Presentation and discussion:

We discuss why a pipeline with correct checks and balances should not be
triggered manually, but automatically instead. We examine different strategies
for building such pipeline and software integration tests.
Finally, we discuss different continuous deployment strategies and examine
real life cases.

Part 3: Practice building a pipeline using gitlab

Deploy a small web application using gitlab pipeline. We set up a small scale
pipeline that will "build" the sources of our application in a form of a Docker
container and then deploy the application to a live Kubernetes cluster.

Part 4: Practice deployment strategies

Exercise deploying different versions and of the application to the cluster
using different deployment strategies and various mechanism. Once, the pipeline
is set up, we can tweak it and use Kubernetes native tools to practice different
deployment strategies. Once we trust Kubernetes native tools,
we examine how we can do the same using Ingress controllers.
