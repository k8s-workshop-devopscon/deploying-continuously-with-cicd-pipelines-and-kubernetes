.. CI\CD with Kubernetes documentation master file, created by
   sphinx-quickstart on Wed May 12 10:39:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CI\CD with Kubernetes's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   parts
   pipeline
   deployments
